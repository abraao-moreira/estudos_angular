import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'http://abraaomoreira.com'
  cursoAngular: boolean = true
  urlImagem: string = 'http://lorempixel.com/400/200/sports/'

  valorAtual: string = ""
  valorSalvo: string = ""

  isMouseOver: boolean = false

  nome: string = "ABC"

  pessoa: any = {
    nome: 'def',
    idade: 20
  }

  nomeDoCurso: string = 'Angular'

  valorInicial: number = 125

  getValor(){
    return 1
  }

  getCurtirCurso(){
    return true
  }

  botaoClicado(){
    alert()
  }

  onKeyUp(evento: KeyboardEvent){
    this.valorAtual = (<HTMLInputElement>evento.target).value
  }

  salvarValor(valor: string){
    this.valorSalvo = valor
  }

  onMouseOverOut(){
    this.isMouseOver = !this.isMouseOver
  }

  onMudouValor(evento: any){
    console.log(evento.novoValor)
  }

  constructor() { }

  ngOnInit(): void {
  }
}
