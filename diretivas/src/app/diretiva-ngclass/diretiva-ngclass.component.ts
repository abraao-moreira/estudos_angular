import { Component, OnInit } from '@angular/core';
import {star, starFill} from 'ngx-bootstrap-icons'

@Component({
  selector: 'app-diretiva-ngclass',
  templateUrl: './diretiva-ngclass.component.html',
  styleUrls: ['./diretiva-ngclass.component.css']
})
export class DiretivaNgclassComponent implements OnInit {

  meuFavorito: boolean = false

  onClick(){
    this.meuFavorito = !this.meuFavorito
  }

  constructor() { }

  ngOnInit(): void {
  }

}
