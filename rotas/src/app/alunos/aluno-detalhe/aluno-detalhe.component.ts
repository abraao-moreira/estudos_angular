import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlunosService } from '../alunos.service';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {

  inscricao: Subscription
  aluno: any

  constructor(
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private _alunosService: AlunosService,
    private _route: ActivatedRoute
  ) {
    // this.inscricao = this._activatedRoute.params.subscribe((params: any) => {
    //   let id = params['id']

    //   this.aluno = this._alunosService.getAluno(id)

      // if(this.aluno == null){
      //   this.router.navigate(['/nao-encontrado'])
      // }

      this.inscricao = this._route.data.subscribe(
        info => {
          this.aluno = info.aluno
        }
      )
   }

  ngOnInit(): void {
    // this.inscricao = this._activatedRoute.params.subscribe((params: any) => {
    //   this.id = params['id']
    // })
  }

  ngOnDestroy(): void {
    this.inscricao.unsubscribe()
  }

  editarContato(){
    this.router.navigate(['/alunos', this.aluno.id, 'editar'])
  }
}
