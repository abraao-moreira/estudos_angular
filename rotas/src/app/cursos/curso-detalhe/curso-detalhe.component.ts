import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CursosService } from '../cursos.service';

@Component({
  selector: 'app-curso-detalhe',
  templateUrl: './curso-detalhe.component.html',
  styleUrls: ['./curso-detalhe.component.css']
})
export class CursoDetalheComponent implements OnInit {

  id: number = 0
  inscricao: Subscription
  curso: any

  constructor(
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private _cursosService: CursosService
  ) {
    this.inscricao = this._activatedRoute.params.subscribe((params: any) => {
      this.id = params['id']

      this.curso = this._cursosService.getCurso(this.id)

      if(this.curso == null){
        this.router.navigate(['cursos/nao-encontrado'])
      }
    })
   }

  ngOnInit(): void {
    // this.inscricao = this._activatedRoute.params.subscribe((params: any) => {
    //   this.id = params['id']
    // })
  }

  ngOnDestroy(): void {
    this.inscricao.unsubscribe()
  }

}
