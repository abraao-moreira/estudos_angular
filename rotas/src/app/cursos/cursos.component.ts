import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CursosService } from './cursos.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  cursos: any[]
  pagina: number = 1
  inscricao: Subscription

  constructor(
    private _cursosService: CursosService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.cursos = this._cursosService.getCursos()
    this.inscricao = this._activatedRoute.queryParams.subscribe(
      (queryParams: any) => {
        this.pagina = queryParams['pagina']
      }
    )
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.inscricao.unsubscribe()
  }

  proximaPagina(){
    // this.pagina++
    this._router.navigate(['/cursos'], {queryParams: {'pagina': ++this.pagina}})
  }

}
