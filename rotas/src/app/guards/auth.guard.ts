import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad{

  constructor(
    private _authService: AuthService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean{

    console.log('AuthGuard')

    return this.verificarAcesso()
  }

  private verificarAcesso(){
    if(this._authService.usuarioEstaAutenticado()){
      return true
    }
    this.router.navigate(['login'])
    return false
  }

  canLoad(
    route: Route
  ): boolean {

    console.log('CanLoad: Verificand se o usuário pode carregar o módulo')

    return this.verificarAcesso()
  }
}
