import { Usuario } from './usuario';
import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  usuarioAutenticado: boolean = false

  mostrarMenuEmitter = new EventEmitter<boolean>()

  constructor(private _router: Router) { }

  fazerLogin(usuario: Usuario){
    if(usuario.nome == 'usuario@email.com' &&
      usuario.senha == '123'){
      this.usuarioAutenticado = true

      this.mostrarMenuEmitter.emit(true)

      this._router.navigate(['/'])
    } else {
      this.usuarioAutenticado = false
    }
  }

  usuarioEstaAutenticado(){
    return this.usuarioAutenticado
  }
}
